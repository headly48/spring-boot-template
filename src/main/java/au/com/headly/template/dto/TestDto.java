package au.com.headly.template.dto;

public class TestDto {
	
	private String username;
	private Long userId;
	
	public TestDto(Long userId) {
		this.userId = userId;		
	}	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
