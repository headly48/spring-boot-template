package au.com.headly.template.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.com.headly.template.dto.TestDto;

@RestController
@RequestMapping(value="/users")
public class TestController {

    @RequestMapping(value="/{user}", method=RequestMethod.GET)
    @ResponseBody
    public TestDto getUser(@PathVariable Long user) {
    	
    	TestDto testDto = new TestDto(user);

    	return testDto;
    }
}